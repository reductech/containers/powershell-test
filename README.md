# reductech/containers/powershell-test

Docker image for testing powershell modules.

[`mcr.microsoft.com/powershell`](https://hub.docker.com/_/microsoft-powershell) with:

* [Pester](https://github.com/pester/Pester)
* [psake](https://github.com/psake/psake)
* [PSScriptAnalyzer](https://github.com/PowerShell/PSScriptAnalyzer)

Current powershell version is `7.0`.

# Build

```powershell
docker build -t reductech/containers/powershell-test .
```

## Using a different base image

It's possible to build using a different base image:

```powershell
docker build --build-arg BASE=mcr.microsoft.com/powershell:windowsservercore-1909 -t reductech/containers/powershell-test:windowsservercore-1909 .
```
